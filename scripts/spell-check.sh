#!/bin/bash


if [[ -z "${DOCKER_MOUNT}" ]];
then
  echo '[ERROR] Set DOCKER_MOUNT to where the source code is mounted'
else
  docker run --entrypoint "" -v $DOCKER_MOUNT:/source registry.gitlab.com/dreamer-labs/dl-lint-docs:dc159d5d-lint-docs /bin/bash -c 'cd source; pip3 install -r requirements.txt; pyspelling --spellchecker hunspell'
fi
